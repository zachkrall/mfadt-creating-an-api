/*
    creating a variable which
    has the date of whenever
    the script runs
*/
const date = new Date();

/* 
   this is a function we will use to calculate
   how many days until graduation   
*/
function getGradDate(currentDate, gradDate){
  let diff = Math.floor( (gradDate - currentDate) / (1000 * 60 * 60 * 24) );

  /*
      our return statement is using template
      literal string syntax which allows us to
      easily insert variables into our strings

      it is important to note that template
      literal strings use backticks (`) which
      are different than quotation marks (' or ")

      documentation: https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Template_literals
  */
  return `${diff} days`;
}

/* 
    We are declaring an object using
    object literal notation
*/
let zach = {
  "first": "Zach",
  "last": "Krall",
  "pronouns": "He/Him",
  "hobbies": [
    "coding", "looking at art", "watching movies",
    "listening to music"
  ],
  "school": "Parsons School of Design",
  "website": "https://zachkrall.com"
};

/*
   If our key has a single word (without spaces) 
   we can use dot notation or bracket notation
   to get the value. 

   example: 
     
     zach.first        // returns "Zach"
     zach["first"]     // returns "Zach"
*/

/*
    To add dynamically created values to
    our object, we can do that individually
    like we would set a variable
*/

zach["full name"]     = zach.first + ' ' + zach.last;
zach["grad countdown"] = getGradDate(date, new Date("05/15/2020"));

/*
    We can use console.log to print
    out our result, just to make sure
    it runs correctly
*/

// console.log( zach );

/* it is currently commented out so that
   it doesn't run when it's imported in our "index.js" script */


/* At the end of our script, we will want to 
   export our object so that it can be imported */
module.exports = zach;