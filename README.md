running `npm install` will install dependencies listed in package.json file.

running `npm start` will run the start script found in the package.json file.
the start script should run index.js which starts our server.

navigating to `localhost:3000/api` in your browser will show you the API response data.