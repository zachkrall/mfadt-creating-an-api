const zach = require('./zach.js');
const app  = require('express')();
const path = require('path');

/*
    Dynamically set the port to use
    based on the running environment
    if running locally, let's set
    the port to be 3000
*/
const PORT = process.env.PORT || '3000';

/*
    we will create two endpoints that
    execute a function when they
    receive a GET request

    the two most common types of requests
    are GET and POST but there are many more
    
    documentation: https://expressjs.com/en/4x/api.html#app.METHOD
*/
app.get('/', function(request, response){
  /*
     when we visit the index of our running
     server process, we will use path.resolve
     to create a file path string that 
     sendFile will use to send the encoded
     bytes of our index.html file
  */
  response.sendFile( path.resolve(__dirname, 'index.html') );
});

app.get('/api', function(request, response){
  /*
     when a call is made to /api
     we want to send the object we
     created and exported in our 
     other file to be sent as a
     json response
     documentation: https://expressjs.com/en/api.html#express.json
  */
  response.json(zach);
});

app.listen(PORT, function(){
  /* 
     This function asks our app to
     listen for any requests that
     come into our designated PORT
  */
  console.log(`App is listening on port ${PORT}`);
});
